<img src="Assets/Images/AlphabetSoupLogo.jpg" alt="Alphabet Soup Logo" width="800"/>

#

This is Gregory Okhuereigbe's response to the Enlighten Programming Challenge (Alphabet Soup).

## Contents
- [About](#About)
- [How It's Made](#How-It's-Made)
- [How It Works](#How-It-Works)
- [Testing](#Testing)

## About

This program helps provide an answer key to a word search embedded in an ASCII text file. The text file contains the number of rows and columns in the word search, the word search character grid, and the words that must be found. 

For more details on the challenge please read [INSTRUCTIONS.md](INSTRUCTIONS.md).

## How It's Made
This program was created with the Eclispe IDE 

<img src="Assets/Images/EclipseLogo.jpg" alt="Eclipse Logo" width="200"/>


written in the Java programming language

<img src="Assets/Images/JavaLogo.jpg" alt="Java Logo" width="50"/>


and utilizes the Gradle automation tool to compile and run unit tests with JUnit

<img src="Assets/Images/GradleLogo.jpg" alt="Gradle Logo" width="110"/><img src="Assets/Images/JUnitLogo.jpg" alt="JUnit Logo" width="150"/>

## How It Works
The program completes the word search and prints the starting and ending indice of each word to console. Example output:
```
DATA 0:0 3:3
RADMF 4:0 4:4
```
The starting and ending indice of each word are found by:
1. Loading/Reading the specified text file
2. Formatting the embedded text (saving the word search character grid values into a 2D array and other values into variables)
3. Searching the character grid for a possible starting letter (remebering that index if a word is later found)
4. Searching the character grid for all next letters of a word (making sure to search in only one direction at a time (the same direction that the next letter of the word is found))
5. Saving the starting and ending indice of the word into a variable
6. Repeating steps 3-6 until the whole 2D character array (the word search character grid) has been traversed
7. Printing the words and its starting/ending indice to console

The code used to do this can be found [here (AlphabetSoup.java)](alphabetSoup/src/main/java/alphabetSoup/AlphabetSoup.java)

This java class uses four primary methods to complete the word search:
- the readFile method (primarily completing steps 1-2 found above)
- the wordSearch method (primarily completing steps 3,5, and 6)
- the searchGrid method (primarily completing step 4)
- the printSolution method (primarily completing step 7)

The other method in the main class is the main method. The main method calls the readFile method, passing the name of a sample text file that I created which contains the sample data provided in the [instructions](INSTRUCTIONS.md). The readFile method will eventually have all other methods called, and the solution will be printed to console. 

It's possible to complete a word search on another specified text file by calling the readFile method from another class and passing the name of the text file (which must exist at the time of the method call) (Ex. AlphabetSoup.readFile("NameOfFile.txt")).

Please read [Testing](#Testing) below for more information on unit tests.

## Testing
A class containing unit tests has been created and is located [here (AlphabetSoupTest.java)](alphabetSoup/src/test/java/alphabetSoup/AlphabetSoupTest.java). 

- The first test checks whether a method in the main class returns the correct ending of a word (the "endingIndexTest" method in the [testing class](alphabetSoup/src/test/java/alphabetSoup/AlphabetSoupTest.java) completes the test) (primarily testing the "searchGrid" method in the [main class](alphabetSoup/src/main/java/alphabetSoup/AlphabetSoup.java))

- The second test checks whether a method in the main class returns the correct solution (comparing the found solution to the actual solution (of the sample data provided in [instructions](INSTRUCTIONS.md))) (the "sampleSolutionTest" method in the [testing class](alphabetSoup/src/test/java/alphabetSoup/AlphabetSoupTest.java) completes the test) (primarily testing the "wordSearch" method in the [main class](alphabetSoup/src/main/java/alphabetSoup/AlphabetSoup.java))

- The third test checks the functionality of the all four major methods in the main class. This is done by calling the readFile method and passing the name of a file. This gives the option to check other word searches that may be saved in other files (as the main class does not only work with the text file containing the sample data) (the "functionalityTest" method in the [testing class](alphabetSoup/src/test/java/alphabetSoup/AlphabetSoupTest.java) completes the test) (primarily testing the "readFile" method in the [main class](alphabetSoup/src/main/java/alphabetSoup/AlphabetSoup.java))