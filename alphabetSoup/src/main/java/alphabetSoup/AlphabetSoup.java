// Created by Gregory Okhuereigbe

package alphabetSoup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

public class AlphabetSoup
{
	// This main class completes a word search on a file called "sampledata.txt".
	// the file contains the sample data found in the instructions of the Alphabet Soup ReadMe
	public static void main(String[] args)
	{
		readFile("sampledata.txt");
	}
	
	// This method loads the stated file and formats the embedded text,
	// then calls other methods to complete the word search and print the solution 
	public static void readFile(String fileName)
	{
		// initializations
		int size = 0;
		char [][] grid = new char[0][0];
		ArrayList<String> wordArrayList = new ArrayList<>();
		
		// attempt to read the file
		try {
			Scanner fileReader = new Scanner(new File(fileName));
			
			String lineOne = fileReader.nextLine();
			size = Character.getNumericValue(lineOne.charAt(0));
			
			grid = new char[size][size];
			
			// This nested loop fills a 2D array with the letters in the word search
			for (int row = 0; row < grid.length; row++)
			{
				String line = fileReader.nextLine();
				int indexCounter = 0;
				
				for (int col = 0; col < grid[row].length; col++)
				{	
					// If we're not looking at the last char in a line, 
					// the indexCounter helps look past the spaces so
					// we're only saving real letters in the 2D array
					if (col != grid[row].length - 1)
					{
						grid[row][col] = line.charAt(indexCounter);
						indexCounter += 2;
					}
					else
					{
						grid[row][col] = line.charAt(indexCounter);
					}
				}
			}
			
			while (fileReader.hasNextLine())
			{
				String word = fileReader.nextLine();
				wordArrayList.add(word);
			}
		} catch (FileNotFoundException error){
			System.out.println("There was an error loading the file.");
			error.printStackTrace();
		}
		
		// Copies the ArrayList as a 1D array that will be passed to other methods
		String[] wordList = new String[wordArrayList.size()];
		
		for (int i = 0; i < wordList.length; i++)
		{
			wordList[i] = wordArrayList.get(i);
		}
		
		// calls a method to search the grid and save the solution to a String[]
		String [] solution = wordSearch(size, grid, wordList);
		
		// calls a method to print the solution
		printSolution(solution);
	}
	
	
	// This method searches the grid for correct words and returns the found solutions
	public static String[] wordSearch(int size, char[][] grid, String[] wordList)
	{	
		String[] endingIndices = new String[wordList.length];   
		String[] fullSolution = new String[wordList.length];
		
		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid[row].length; col++)
			{
				// This loop helps check for the 1st letter of each word at the current 
				// index of the 2D array
				for (int wordNum = 0; wordNum < wordList.length; wordNum++)
				{
					if (grid[row][col] == wordList[wordNum].charAt(0))
					{
						endingIndices[wordNum] = searchGrid(wordList[wordNum], 0, row, col, grid, "");
						
						if (endingIndices[wordNum] != "")
						{
							fullSolution[wordNum] = wordList[wordNum] + " " + row + ":" + col + " " + endingIndices[wordNum];
						}
					}
				}
			}
		}
		
		return fullSolution;
	}
	
	
	// This is a recursive method that finds the location of the next character of a word
	// until the last character of the word is found.
	public static String searchGrid(String word, int wordIndex, int row, int col, char[][] grid, String endingIndex)
	{	
		//Boundary check: Making sure we're in the grid and looking at a possible next character
		if (row < 0 || row >= grid.length ||
			col < 0 || col >= grid[row].length ||
			grid[row][col] != word.charAt(wordIndex))
		{
			return endingIndex;
		}
		
		// When the last letter is found, the location of the last letter of the word is returned
		if (wordIndex == (word.length() - 1))
		{
			endingIndex = row + ":" + col;
			return endingIndex;
		}
		
		// These are the initial calls to this same method checking for the second letter of
		// the current word in all adjacent locations.
		if (wordIndex == 0)
		{
			wordIndex += 1;
			
			endingIndex = searchGrid(word, wordIndex, row - 1, col, grid, endingIndex); // checks ABOVE
			endingIndex = searchGrid(word, wordIndex, row + 1, col, grid, endingIndex); // checks BELOW
			endingIndex = searchGrid(word, wordIndex, row, col - 1, grid, endingIndex); // checks LEFT
			endingIndex = searchGrid(word, wordIndex, row, col + 1, grid, endingIndex); // checks RIGHT
			endingIndex = searchGrid(word, wordIndex, row - 1, col - 1, grid, endingIndex); // checks DIAG-UP/LEFT
			endingIndex = searchGrid(word, wordIndex, row - 1, col + 1, grid, endingIndex); // checks DIAG-UP/RIGHT
			endingIndex = searchGrid(word, wordIndex, row + 1, col - 1, grid, endingIndex); // checks DIAG-DOWN/LEFT
			endingIndex = searchGrid(word, wordIndex, row + 1, col + 1, grid, endingIndex); // checks DIAG-DOWN/RIGHT
		}
			
		//wordIndex += 1;
		
		
		// The following if statements make sure we're searching for the correct next letter
		// in the same direction we just came from: up, down, left, right, or diagonally up/left, up/right, 
		// down/left, down/right
		
		// English translation:
		// "If the letter below the current letter is the previous letter of the word,
		// keep looking upward"
		if (row < grid.length - 1) // boundary check
		{
			if (grid[row + 1][col] == word.charAt(wordIndex - 1))     // if next letter is below
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row - 1, col, grid, endingIndex); // check ABOVE
			}
		}
		
		// English translation:
		// "If the letter above the current letter is the previous letter of the word,
		// keep looking downward"
		if (row >= 1) // boundary check
		{
			if (grid[row - 1][col] == word.charAt(wordIndex - 1))     // if above
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row + 1, col, grid, endingIndex); // check BELOW
			}
		}
		
		// and so on...
		if (col < grid[row].length - 1) // boundary check
		{
			if (grid[row][col + 1] == word.charAt(wordIndex - 1))     // if right
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row, col - 1, grid, endingIndex); // check LEFT
			}
		}
		
		if (col >= 1)
		{
			if (grid[row][col - 1] == word.charAt(wordIndex - 1))     // if left
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row, col + 1, grid, endingIndex); // check RIGHT
			}
		}
		
		if (row < grid.length - 1 && col < grid[row].length - 1)
		{
			if (grid[row + 1][col + 1] == word.charAt(wordIndex - 1)) // if previous letter is located diagonally-down/right
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row - 1, col - 1, grid, endingIndex); // check DIAG-UP/LEFT
			}
		}
		
		if (row < grid.length - 1 && col >=1)
		{
			if (grid[row + 1][col - 1] == word.charAt(wordIndex - 1)) // if diagonally-down/left
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row - 1, col + 1, grid, endingIndex); // check DIAG-UP/RIGHT
			}
		}	
		
		if (row >= 1 && col < grid[row].length - 1)
		{
			if (grid[row - 1][col + 1] == word.charAt(wordIndex - 1)) // if diagonally-up/right
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row + 1, col - 1, grid, endingIndex); // check DIAG-DOWN/LEFT
			}
		}
		
		if (row >= 1 && col >= 1)
		{
			if (grid[row - 1][col - 1] == word.charAt(wordIndex - 1)) // if diagonally-up/left
			{
				wordIndex += 1;
				endingIndex = searchGrid(word, wordIndex, row + 1, col + 1, grid, endingIndex); // check DIAG-DOWN/RIGHT
			}
		}
		
		
		
		return endingIndex;
	}
	
	// This method prints the solutions
	public static void printSolution(String[] solution)
	{	
		for (int i = 0; i < solution.length; i++)
		{
			System.out.println(solution[i]);
		}
	}
}
