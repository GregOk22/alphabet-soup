// Created by Gregory Okhuereigbe

package alphabetSoup;

import org.junit.Test;

public class AlphabetSoupTest 
{
	// This is a test that checks whether the correct location of the last letter of a word is returned
	// (tests the searchGrid method)
	@Test
	public void endingIndexTest()
	{
		System.out.println("End of Word Index Corectness Test:");
		
		char [][] grid = { {'X','X','X','X','F'} , 
		   		   	  	   {'X','X','X','M','X'} , 
		   		   	  	   {'X','X','D','X','X'} , 
		   		   	  	   {'X','A','X','X','X'} ,
			   	   	  	   {'R','X','X','X','X'},
			   	   	  	   {'X','D','A','T','A'} };
		
		String testWord = "RADMF";
			
		String foundEndingIndex = "";
			
		String trueEndingIndex = "0:4";
		
		
		System.out.println("  comparing found ending index with true ending index...");	
		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid[row].length; col++)
			{
				if (grid[row][col] == testWord.charAt(0))
				{
					foundEndingIndex = AlphabetSoup.searchGrid(testWord, 0, row, col, grid, "");
					
					if (!foundEndingIndex.equals(""))
					{
						if (foundEndingIndex.equals(trueEndingIndex))
						{
							System.out.println("The correct end of word index is being returned!\n");
						}
						else
						{
							System.out.println("There was a problem obtaining the correct end of word index.\n");
							System.out.println(foundEndingIndex);
						}
					}
					else
					{
						System.out.println("Theres a problem finding a word.\n");
					}
				}
			}
		}
	}
	
	
	@Test
	// This is a test that checks whether the correct solution is found
	// (tests the wordSearch method)
	public void sampleSolutionTest()
	{
		System.out.println("\nSolution Corectness Test:\n"
						+ "  comparing found solution and true solution...");
		int size = 5;
		
		char [][] grid = { {'H','A','S','D','F'} , 
				   		   {'G','E','Y','B','H'} , 
				   		   {'J','K','L','Z','X'} , 
				   		   {'C','V','B','L','N'} , 
				   		   {'G','O','O','D','O'} };
		
		String[] wordList = {"HELLO", "GOOD", "BYE"};
		
		String[] foundSolution = AlphabetSoup.wordSearch(size, grid, wordList);
		
		String[] trueSolution = {"HELLO 0:0 4:4", "GOOD 4:0 4:3", "BYE 1:3 1:1"};
		
		if (foundSolution[0].equals(trueSolution[0]) && 
			foundSolution[1].equals(trueSolution[1]) && 
			foundSolution[2].equals(trueSolution[2]) )
		{
			System.out.println("The correct solution was found!\n");
		}
		else
		{
			System.out.println("An incorrect solution was found.\n");
			System.out.println(foundSolution[0]);
			System.out.println(foundSolution[1]);
			System.out.println(foundSolution[2]);
		}
	}
	
	
	// This is a test that checks the functionality of the four major methods: 
	// (readFile, wordSearch, searchGrid, printSolution)
	// (calls the readFile method (which communicates with the other methods))
	@Test
	public void functionalityTest()
	{
		System.out.println("\nThe found solution is below:\n");
		AlphabetSoup.readFile("sampledata.txt");
		
		// if the readFile method successfully read and formatted the file
		// into readable pieces of data that the other methods can read, 
		// the correct solution will be printed
	}
}